mark = {}
for i in open("studentRanking.txt", 'r'):
    stud = i.split()
    mark[stud[0]] = [int(_) for _ in stud[1:]]

#This funtions does sorting of the ranks in acsending order for given subject

def order(mark: list[list[int]], n: int) -> list[list[int]]:
    for _ in range(len(mark)):
        for subj in range(len(mark) -1):
            if mark[subj][n] > mark[subj + 1][n]:
                mark[subj], mark[subj + 1 ] =  mark[subj + 1],  mark[subj] 
    return mark

def subj_rank(mark_dict: dict, ranked_marks: list[list[int]]) -> list[str]:
    rank = [0] * len(mark_dict)
    for subj in range(len(ranked_marks)):
        for name in mark_dict.keys():
            if mark_dict[name] == ranked_marks[subj]:
                rank[subj] = name
    return rank

def each_subj_rank(mark: dict, no_of_subj: int = 3) -> list[list[str]]:
    name_ranks = []
    for _ in range(no_of_subj):
        name_ranks.append(subj_rank(mark, order(list(mark.values()), _)))
    return name_ranks


#print(order(list(mark.values()), 0))
#print(subj_rank(mark, order(list(mark.values()), 0)))

print(each_subj_rank(mark))
